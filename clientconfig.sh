# First arg: client name

cat client.conf \
    <(echo -e '<ca>') \
    public/ca.crt \
    <(echo -e '</ca>\n<cert>') \
    output/${1}.crt \
    <(echo -e '</cert>\n<key>') \
    output/${1}.key \
    <(echo -e '</key>\n<tls-auth>') \
    output/ta.key \
    <(echo -e '</tls-auth>') \
    > output/${1}.ovpn

echo "Remember to update 'remote' and 'resolv-conf' lines in the output!"

#! /usr/bin/env python3

# Copyright (C) 2014 Sam Lade
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import os
import shutil
import subprocess

def gen_key(path, algo="ecdsa"):
    if algo == "ecdsa":
        subprocess.check_call(["openssl", "ecparam", "-out", path, "-name", "secp521r1", "-genkey"])
    elif algo == "rsa":
        subprocess.check_call(["openssl", "genpkey", "-out", path, "-algorithm", "RSA", "-pkeyopt", "rsa_keygen_bits:4096"])
    else:
        raise ValueError("Unsupported algorithm: {}".format(algo))

cadirs = ["output", "public", "private", "conf", "signed-certificates"]
def init(args):
    # make directories
    for i in cadirs:
        if not os.path.isdir(i):
            os.mkdir(i)
    # write CA creation config file
    open("conf/cagen.conf", "w").write("""
        [req]
        prompt = no
        x509_extensions = v3_ca
        distinguished_name = root_ca_distinguished_name

        [root_ca_distinguished_name]
        countryName = UK
        stateOrProvinceName = London
        commonName = VPN CA

        [v3_ca]
        basicConstraints = CA:true
    """)
    # generate key
    gen_key("private/ca.key", "rsa" if args.rsa else "ecdsa")
    # generate CA cert
    subprocess.check_call(["openssl", "req", "-new", "-nodes", "-days", "1825", "-x509", "-key", "private/ca.key", "-out", "public/ca.crt", "-config", "conf/cagen.conf", "-sha512"])
    # clean CA creation config file
    os.remove("conf/cagen.conf")

    # write CA config file
    open("conf/ca.conf", "w").write("""
        [ca]
        default_ca = CA_default

        [CA_default]
        dir = .
        new_certs_dir = ./signed-certificates/
        database = ./conf/index
        certificate = ./public/ca.crt
        serial = ./conf/serial
        private_key = ./private/ca.key
        x509_extensions = usr_client_cert
        name_opt = ca_default
        cert_opt = ca_default
        default_days = 365
        default_md = sha512
        preserve = no
        policy = policy_match
        unique_subject = no

        [policy_match]
        countryName = match
        stateOrProvinceName = match
        commonName = supplied
        emailAddress = optional

        [usr_client_cert]
        basicConstraints = CA:false
        keyUsage = digitalSignature
        extendedKeyUsage=clientAuth

        [usr_server_cert]
        basicConstraints = CA:false
        keyUsage = digitalSignature,keyEncipherment
        extendedKeyUsage=serverAuth
    """)
    # create serial number file and index
    open("conf/serial", "w").write("01")
    open("conf/index", "w")
    # done!
    print("Success. CA certificate: public/ca.crt")

def client(args):
    name = args.name.replace(" ", "_")
    # write CSR config
    open("output/csr-{}.conf".format(name), "w").write("""
        [req]
        prompt = no
        distinguished_name = client_distinguished_name
        
        [client_distinguished_name]
        countryName = UK
        stateOrProvinceName = London
        commonName = {} VPN
    """.format(args.name))
    # generate key
    gen_key("output/{}.key".format(name), "rsa" if args.rsa else "ecdsa")
    # generate CSR
    subprocess.check_call(["openssl", "req", "-new", "-nodes", "-days", "365", "-key", "output/{}.key".format(name), "-out", "output/{}.csr".format(name), "-config", "output/csr-{}.conf".format(name), "-sha512"])
    # clean CSR config
    os.remove("output/csr-{}.conf".format(name))
    # sign CSR
    if args.server:
        usage = "usr_server_cert"
    else:
        usage = "usr_client_cert"
    subprocess.check_call(["openssl", "ca", "-in", "output/{}.csr".format(name), "-config", "conf/ca.conf", "-extensions", usage, "-batch", "-out", "output/{}.crt".format(name), "-notext"])
    # clean CSR
    os.remove("output/{}.csr".format(name))
    # return pub,priv key paths
    print("Success. Private key: output/{0}.key Certificate: output/{0}.crt".format(name))

def clean(args):
    for path in cadirs:
        shutil.rmtree(path)

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command", title="command")
subparsers.required = True

init_parser = subparsers.add_parser("init", help="create a new CA")
init_parser.set_defaults(func=init)

client_parser = subparsers.add_parser("client", help="create a new client certificate")
client_parser.add_argument("name", help="client name")
client_parser.add_argument("-s", "--server", action="store_true", help="mark this as a server certificate")
client_parser.set_defaults(func=client)

clean_parser = subparsers.add_parser("clean", help="delete the entire CA")
clean_parser.set_defaults(func=clean)

parser.add_argument("-r", "--rsa", action="store_true", help="use RSA rather than ECDSA")

args = parser.parse_args()
args.func(args)

openssl dhparam -out output/dh2048.pem 2048
openvpn --genkey --secret output/ta.key
#cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/
#gunzip /etc/openvpn/server.conf.gz
cp server.conf output/dh.pem public/ca.crt output/server.* output/ta.key /etc/openvpn
cd /etc/openvpn

#cat 'push "redirect-gateway def1 bypass-dhcp"' >> server.conf
#cat 'push "dhcp-option DNS 1.1.1.1"' >> server.conf
#cat 'push "dhcp-option DNS 1.0.0.1"' >> server.conf
#cat 'tls-auth ta.key 0' >> server.conf
#cat 'tls-cipher TLS-DHE-RSA-WITH-AES-128-GCM-SHA256' >> server.conf
#cat 'user nobody' >> server.conf
#cat 'group nobody' >> server.conf

cat net.ipv4.ip_forward=1 >> /etc/sysctl.conf

cat '*nat' >> /etc/ufw/before.rules
cat '-A POSTROUTING -s 10.8.0.0/8 -o eth0 -j MASQUERADE' >> /etc/ufw/before.rules
cat 'COMMIT' >> /etc/ufw/before.rules

sed -i "s/DEFAULT_FORWARD_POLICY=\"DROP\"/DEFAULT_FORWARD_POLICY=\"ACCEPT\"/" /etc/default/ufw

ufw allow 1194/udp
ufw allow OpenSSH

ufw disable
ufw enable

systemctl start openvpn@server
systemctl enable openvpn@server
